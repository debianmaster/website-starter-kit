package webroute

import (
	"github.com/pkg/errors"
	"net/url"
)

type WebRoute struct {
	webAppUrl url.URL
}

func New(appBaseUrl string) (WebRoute, error) {
	var r WebRoute

	appUrl, err := url.Parse(appBaseUrl)
	if err != nil {
		return r, errors.WithMessagef(err, "Failed to parse app base URL '%s'", appBaseUrl)
	}
	r.webAppUrl = *appUrl

	return r, nil
}

func (r WebRoute) WebsiteUrl(urlPath string) string {
	u := r.webAppUrl
	u.Path = urlPath
	return u.String()
}
